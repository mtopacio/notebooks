# Google Colab Notebooks
Collection of Python notebooks used to explore different packages or learning topics. Within this repo are the output of each `ipynb`; however, below are the links to the actual notebooks you can view under _Google Colab_. 

_datasets_ hold files that were used and _literature_ holds any refernces as a `pdf`.

If referencing links outside, the following will include ![Open In Colab](https://colab.research.google.com/assets/colab-badge.svg):
`[![Open In Colab](https://colab.research.google.com/assets/colab-badge.svg)]`


[CRISP-DM](https://www.datascience-pm.com/crisp-dm-2/)

Other Resources:
- [BioStudies](https://www.ebi.ac.uk/biostudies/): a database that hold descriptions of biological studies and links to data or supplementary information either in EMBL-EBI or an external source
- [F1000Research](https://f1000research.com/): an open access publishing platform supporting data depostiion and sharing

## No Code Summaries
- [Bioinformatics](https://colab.research.google.com/drive/1-FNa5uCZd9_fRnHlj5psdXGCmry6gzTe?usp=sharing)
- [AWS CCP](https://colab.research.google.com/drive/19T0DN1Si92zP-ei6XAtfMBhx7Ws1QVlH?usp=sharing)

## Sections
1. [Statistics](##Statistics)
2. [Machine Learning](##Machine Learning)
3. [Databases](##Databases)
4. [Visualizations](##Visualizations)
5. [NLP](##NLP)
6. [Bioinformatics](##Bioinformatics)
7. [Web Scraping](##Web Scraping)

---

## Statistics
- [EDA](https://colab.research.google.com/drive/1gcJ-UMip7FdgPiHkCqSMKj6AQLuEUfWZ?usp=sharing)
- [EDA in R](https://colab.research.google.com/drive/1HLnXlAC6sNHiSvAKXRP6n7F3W3V1R2po?usp=sharing)
- [Time Series](https://colab.research.google.com/drive/19NUL2zSFHa00IZHabgaDR1wjMaqHLyoX?usp=sharing)

## Machine Learning
- Tensorflow
- Scikit Learn
- PyTorch

## Databases
- [SQLite3](https://colab.research.google.com/drive/1vHmrR0kEYmBkNdO_11K1mCZF6fJXUABz?usp=sharing)
- [PostgreSQL](https://colab.research.google.com/drive/1vZuEc3WtphxDefYRAxuEMF5AX3Lt9gDa?usp=sharing) 
- [PostgreSQL with Python](https://colab.research.google.com/drive/1mSBB5-z56RrQvk3ukK7Z3ghKoDi21BPJ?usp=sharing)
- [MongoDB Atlas](https://colab.research.google.com/drive/1Cp-EeOmSoRPMPVFZ8pmgVvGrvkOsvWFr?usp=sharing)
- [Neo4j and CQL](https://colab.research.google.com/drive/1oOlT8yTaxAa5o-81VWtvv08FcIPhZtcu?usp=sharing)

## Visualizations
- [Dash](https://colab.research.google.com/drive/1xoErhasFOXWoVWIrfp0kRtUHmVg_7gjJ?usp=sharing)

## NLP
- [NLTK](https://colab.research.google.com/drive/13VB992tOZfTl_UsCw-cITfhDrTKoyKc8?usp=sharing)
- [Classification (Naive-Bayes, SVC, Deep Learning)](https://colab.research.google.com/drive/19zjzR7jH0L5bnZoNW5emDk6AF1a_5-kH?usp=sharing)

## Bioinformatics
- ### Projects/Resources
    - [Tracespip](https://github.com/viromelab/tracespipe)
    - [FlowBio](https://github.com/NCBI-Hackathons/FlowBio)

- [Variant Call Format](https://colab.research.google.com/drive/1ihQNUAfQCtk9wgr62zT-5A6gi0pmk7iA?usp=sharing) 
- [HTSlib](https://colab.research.google.com/drive/1ck4aN5gDw-anpaDB7SZZyL12cf2Qkcc-?usp=sharing) (SAMtools and BCFtools)

## Web
- [Using Proxies](https://colab.research.google.com/drive/1t94zjOiX2IYsGrIOsscdDE46eYxpP-lo?usp=sharing) 
- [Historical Values of Cryptocurrencies with Requests/BeautifulSoup](https://colab.research.google.com/drive/1363MoWv5C-9bQm71xV-rN2Qlwim4_Bor?usp=sharing) 
- [Searching _Amazon_ and scraping pictures](https://colab.research.google.com/drive/1Q1tA0cNrjX8u6Fp6BNMyRATwX9NYBC2-?usp=sharing)
- [Fast API](https://colab.research.google.com/drive/1GAoNFNzhDfr69Vx1JPawFOo0JbXTlH6k?usp=sharing)

## Other
- [Debian vs. Arch Packages](https://colab.research.google.com/drive/1X4n0KYV1_ggf_UDgUKh8YUnxwk87Mi-J?usp=sharing)

## Open Data on AWS
- [Terrain Tiles](https://registry.opendata.aws/terrain-tiles/)
- [Southern California Earthquakes](https://registry.opendata.aws/southern-california-earthquakes/)
- [Copernicus Digital Elevation Model (DEM)](https://registry.opendata.aws/copernicus-dem/)
- [Open City Model](https://registry.opendata.aws/opencitymodel/)
- [Covid Job Impacts](https://registry.opendata.aws/us-hiring-rates-pandemic/)
